package com.example.password

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.children
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.password.adapters.RecyclerViewAdapter
import com.example.password.databinding.ActivityMainBinding
import com.example.password.models.ButtonSrc

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: RecyclerViewAdapter
    private val items = mutableListOf<ButtonSrc>()

    private var passcode = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        init()
        setContentView(binding.root)
    }

    private fun init(){
        setData()
        adapter = RecyclerViewAdapter(items, object : OnItemListener{
            override fun itemOnClick(position: Int, number: String?, srcId: Int?) {
                if (number == null && srcId != null && srcId == R.drawable.ic_delete){
                    if (passcode.isNotEmpty()){
                        passcode.removeAt(passcode.size - 1)
                        passwordChecker(passcode.size)
                    }
                }else if (number != null){
                    if (passcode.size < 4)
                        passcode.add(number)
                        passwordChecker(passcode.size)
                }
            }
        })
        binding.recyclerViewForNumbers.layoutManager = GridLayoutManager(this, 3)
    }

    private fun passwordChecker(passcodeLen: Int){
        binding.linearLittleCircles.children.forEach {
            for(i in 0..passcodeLen){
                it.setBackgroundResource(R.drawable.after_click_circle)
            }
        }
    }

    private fun setData(){
        items.add(ButtonSrc(1, null))
        items.add(ButtonSrc(2, null))
        items.add(ButtonSrc(3, null))
        items.add(ButtonSrc(4, null))
        items.add(ButtonSrc(5, null))
        items.add(ButtonSrc(6, null))
        items.add(ButtonSrc(7, null))
        items.add(ButtonSrc(8, null))
        items.add(ButtonSrc(9, null))
        items.add(ButtonSrc(null, R.drawable.ic_touch__id))
        items.add(ButtonSrc(0, null))
        items.add(ButtonSrc(null, R.drawable.ic_delete))
    }
}