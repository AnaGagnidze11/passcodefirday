package com.example.password

interface OnItemListener {
    fun itemOnClick(position: Int, number: String?, srcId: Int? = null)
}