package com.example.password.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.password.OnItemListener
import com.example.password.databinding.NumberItemLayoutBinding
import com.example.password.databinding.SrcItemLayoutBinding
import com.example.password.models.ButtonSrc

class RecyclerViewAdapter(private val items: List<ButtonSrc>, private val onItemListener: OnItemListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object{
        private const val NUMBER = 1
        private const val SRC = 2
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == NUMBER)
            NumberViewHolder(NumberItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        else
            SrcViewHolder(SrcItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is NumberViewHolder -> holder.bindNumber()
            is SrcViewHolder -> holder.bindSrc()
        }
    }

    override fun getItemCount() = items.size

    inner class NumberViewHolder(private val binding: NumberItemLayoutBinding): RecyclerView.ViewHolder(binding.root), View.OnClickListener{
        private lateinit var item: ButtonSrc
        fun bindNumber(){
            item = items[adapterPosition]
            binding.numberBtn.text = item.number.toString()
        }

        override fun onClick(v: View?) {
            onItemListener.itemOnClick(adapterPosition, item.number.toString())
        }
    }

    inner class SrcViewHolder(private val binding: SrcItemLayoutBinding): RecyclerView.ViewHolder(binding.root), View.OnClickListener{
        private lateinit var item: ButtonSrc
        fun bindSrc(){
            item = items[adapterPosition]
            item.src?.let { binding.srcImgBtn.setImageResource(it) }
        }

        override fun onClick(v: View?) {
            onItemListener.itemOnClick(adapterPosition, null, item.src)
        }
    }


    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return if (item.src == null) NUMBER
        else SRC
    }
}